var express = require('express');
var http = require("http");
var formidable = require("formidable");
var util = require("util");
const path = require('path');

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  

app.use('/', express.static(path.join(__dirname, 'public')));

app.post('/postEmployee', function(req, res, next) {
    processForm(req,res);
    next();
   });

app.get('/get-languages',function(req,res,next){
    getLanguages(req,res);
    next();

    // var data = {
    //         data: { 
    //             languages: [
    //             'English',
    //             'Spanish',
    //             'German',
    //             'Other'
    //             ]        
    //         }
    //     };

    //     var responseData = JSON.stringify(data);
    //     console.log('get: ', responseData);
    //     res.end(responseData);
})


// var server = http.createServer(function(req, res) {
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.setHeader(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );

//   if (req.method.toLowerCase() == "post") {
//     processForm(req, res);
//     return;
//   }

//   if (req.method.toLowerCase() =='get') {
//       var data = {
//             data: { 
//                 languages: [
//                 'English',
//                 'Spanish',
//                 'German',
//                 'Other'
//                 ]        
//             }
//       };

//       var responseData = JSON.stringify(data);
//       res.end(responseData);
//       console.log('get: ', responseData);
//       return;
//   }

//   res.end();
// });

function processForm(req, res) {
  var form = new formidable.IncomingForm();

  form.parse(req,function(err,fields) {

    // give employee  an arbitrary id
    fields.id = 'ABC123';

    var data = JSON.stringify({
      data: fields
    });

    console.log("posted fields: ");
    console.log(data);

  
  });


  
    

    

//   form.parse(req, function(err, fields) {
//     res.writeHead(200, {
//       "content-type": "text/plain"
//     });

//     // give employee  an arbitrary id
//     fields.id = 'ABC123';

//     var data = JSON.stringify({
//       data: fields
//     });

//     res.end(data);

//     console.log("posted fields: ");
//     console.log(data);
//   });



}

function getLanguages(req,res) {
    var data = {
        data: { 
            languages: [
            'English',
            'Spanish',
            'German',
            'Other'
            ]        
        }
    };

    var responseData = JSON.stringify(data);
    console.log('get: ', responseData);
    res.end(responseData);
}

var port = 3100;
app.listen(port, () => {
    console.log('Server app listening on port 3100!')
});
// server.listen(port);
// console.log("server listening on port " + port);
